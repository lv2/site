Title:
save_as: index.html

<img alt="LV2 logo" class="biglogo" src="images/logo.svg"></img>

LV2 is an extensible open standard for audio plugins.
LV2 has a simple core interface,
which is accompanied by extensions that add more advanced functionality.

Many types of plugins can be built with LV2, including audio effects, synthesizers, and control processors for modulation and automation.
Extensions support more powerful features, such as:

 * Platform-native UIs
 * Network-transparent plugin control
 * Portable and archivable persistent state
 * Non-realtime tasks (like file loading) with sample-accurate export
 * Semantic control with meaningful control designations and value units

The LV2 specification and accompanying code is permissively licensed free software, with support for all major platforms.

Documentation
-------------

* [Why LV2?](pages/why-lv2.html)
* [Developing with LV2](pages/developing.html)
* [LV2 Specifications](//lv2plug.in/ns/)
* [Host Compatibility](pages/host-compatibility.html)
* [News](/news.html)

Community
---------

* [Projects using LV2](pages/projects.html)
* [Mailing list](http://lists.lv2plug.in/listinfo.cgi/devel-lv2plug.in)
* [Chat](https://web.libera.chat/#lv2)

Developing
----------

* [Download LV2 1.18.10](//lv2plug.in/spec/lv2-1.18.10.tar.xz)<a class="siglink" href="//lv2plug.in/spec/lv2-1.18.10.tar.xz.sig">&nbsp;&#10003;</a>
* [Repository](http://gitlab.com/lv2/lv2)
* [Issues](https://gitlab.com/lv2/lv2/-/issues)
