Title: LV2 1.18.8
Date: 2022-08-12 00:00
Slug: lv2-1-18-8
Author: drobilla

[LV2 1.18.8](https://lv2plug.in/spec/lv2-1.18.8.tar.xz) has been released.  LV2 is a plugin standard for audio systems.  It defines an extensible C API for plugins, and a format for self-contained "bundle" directories that contain plugins, metadata, and other resources.  See <http://lv2plug.in/> for more information.

Changes:

 * Fix documentation build with Python 3.7.
 * Fix documentation build with meson 0.56.2.
 * Fix lv2.h missing from installation.
 * eg-midigate: Fix output timing.
 * eg-sampler: Add resampling via libsamplerate.
 * eg-sampler: Fix potentially corrupt notification events.
 * lv2core: Fix inconsistent plugin class labels.
 * lv2specgen: Fix installed data paths.
