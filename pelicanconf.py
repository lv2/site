AUTHOR = 'David Robillard'
SITENAME = 'LV2'
SITELOGO = 'images/logo.svg'
SITELOGO_WIDTH = '13.097287em'
SITELOGO_HEIGHT = '12.2625em'

PATH = 'content'

TIMEZONE = 'America/Toronto'

DEFAULT_LANG = 'en'

THEME = 'themes/lv2'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = ()

# Social widget
SOCIAL = ()

# Menu

DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False

DEFAULT_PAGINATION = False
SUMMARY_MAX_LENGTH = None

DIRECT_TEMPLATES = ['news']

PAGINATED_TEMPLATES = {
    'author': 10,
    'category': 10,
    'index': None,
    'news': 10,
    'tag': 10,
}

ARTICLE_PATHS = ['news']
STATIC_PATHS = ['images', 'favicon.ico']

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
